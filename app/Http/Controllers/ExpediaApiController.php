<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use StdClass;
use App\Libraries\Filter;
use App\Libraries\EroamSession;
use App\Libraries\ApiCache;
use PDF;
use Mail;
use Cache;
use Validator;
use App\Libraries\Map;
use Carbon\Carbon;
use Config;
class ExpediaApiController extends Controller {

    public function getHotelDetails($hotelId, $arrivalDate, $departureDate, $room, $roomTypeCode, $includeDetails, $options, $rateKey, $latitude, $longitude, $customerSessionId, $type = 1, $rateCode = '') {

        $hotel_detail_data = array();
        $hotel_detail_data['code'] = $hotelId;
        $hotel_detail_data['arrivalDate'] = $arrivalDate;
        $hotel_detail_data['departureDate'] = $departureDate;
        $hotel_detail_data['room'] = $room;
        $hotel_detail_data['roomTypeCode'] = $roomTypeCode;
        $hotel_detail_data['includeDetails'] = $includeDetails;
        $hotel_detail_data['options'] = $options;
        $hotel_detail_data['rateKey'] = $rateKey;
        $hotel_detail_data['latitude'] = $latitude;
        $hotel_detail_data['longitude'] = $longitude;
        $hotel_detail_data['customerSessionId'] = $customerSessionId;
        $hotel_detail_data['type'] = 1;
        $hotel_detail_data['rateCode'] = '';
        $hotel_detail_data['customeruseragent'] = getBrowserName();

        //echo '<pre>'; print_r(json_encode($hotel_detail_data)); die;
        $response = http('post', 'expedia/get-hotel-details', $hotel_detail_data);
        $result = '';
        if (!empty($response)) {
            $result = json_decode(json_encode($response));
        }
        ///echo '<pre>'; print_r($result);  die;
        return $result;
    }
    public function hotelInfo($hotelID) {
        $hotel_detail_data = array();
        $hotel_detail_data['hotelID'] = $hotelID;
        $hotel_detail_data['countryCode'] = 'AU';
        $hotel_detail_data['customeruseragent'] = getBrowserName();

        $response = http('post', 'expedia/get-hotel-info', $hotel_detail_data);
        $result  = array();
        $result['data'] = $response;
        return $result;
    }
    //check price variation for expedia
    public function expediaPriceCheck() {
        $data = session()->get('search');
        $priceCheck = array();
        $num_of_rooms = session()->get('search_input') ['rooms'];
        $hotel_request_data = session()->get('search_input');
        $children = array();
        if (isset($hotel_request_data['child']) && !empty($hotel_request_data['child'])) {
            $children = $hotel_request_data['child'];
        }
        $expedia_request = '';
        if ($num_of_rooms == 1) {
            $expedia_request.= '&room1=' . session()->get('search_input') ['num_of_adults'][0];
            if (isset($children[0]) && !empty($children[0])) {
                $c_array = implode(',', $children[0]);
                $c_array = str_replace('0', '1', $c_array);
                $expedia_request.= ',' . count($children[0]) . ',' . $c_array;
            }
        } else {
            for ($i = 0;$i < $num_of_rooms;$i++) {
                $n = $i + 1;
                $expedia_request.= '&room' . $n . '=' . session()->get('search_input') ['num_of_adults'][$i];
                if (isset($children[$i]) && !empty($children[$i])) {
                    $c_array = implode(',', $children[$i]);
                    $c_array = str_replace('0', '1', $c_array);
                    $expedia_request.= ',' . count($children[$i]) . ',' . $c_array;
                }
            }
        }
        $session_data = session()->get('search');
        foreach ($session_data['itinerary'] as $key => $value) {
            if ($value['hotel'] &&  $value['hotel']['provider'] == 'expedia') {
                $value['hotel'] = json_decode(json_encode($value['hotel']), true);
                $nights = $value['city']['default_nights'];
                $code = $value['hotel']['hotelId'];
                $arrivalDate = date('m-d-Y', strtotime($value['hotel']['checkin']));
                $departureDate = date('m-d-Y', strtotime($value['hotel']['checkout']));
                $room = $expedia_request;
                if (isset($value['hotel']['RoomRateDetailsList']['RoomRateDetails']['roomTypeCode'])) {
                    $roomTypeCode = $value['hotel']['RoomRateDetailsList']['RoomRateDetails']['roomTypeCode'];
                } else {
                    $roomCode = '@roomCode';
                    $roomTypeCode = $value['hotel']['RoomRateDetailsList']['RoomRateDetails']['RoomType'][$roomCode];
                }
                $rateCode = $value['hotel']['RoomRateDetailsList']['RoomRateDetails']['rateCode'];
                $includeDetails = 'true';
                $options = 'HOTEL_DETAILS,ROOM_TYPES,ROOM_AMENITIES,PROPERTY_AMENITIES,HOTEL_IMAGES';
                if (isset($value['hotel']['RoomRateDetailsList']['RoomRateDetails']['RateInfos']['RateInfo']['RoomGroup']['Room'][0]['rateKey'])) {
                    $rateKey = $value['hotel']['RoomRateDetailsList']['RoomRateDetails']['RateInfos']['RateInfo']['RoomGroup']['Room'][0]['rateKey'];
                } else {
                    $rateKey = $value['hotel']['RoomRateDetailsList']['RoomRateDetails']['RateInfos']['RateInfo']['RoomGroup']['Room']['rateKey'];
                }
                $rateKey = $rateKey;
                $latitude = $value['hotel']['latitude'];
                $longitude = $value['hotel']['longitude'];
                $customerSessionId = $value['hotel']['customerSessionId'];
                $response = $this->getHotelDetails($code, $arrivalDate, $departureDate, $room, $roomTypeCode, $includeDetails, $options, $rateKey, $latitude, $longitude, $customerSessionId);
                $selected_hotel = json_decode(json_encode($value['hotel']), true);
                $total = '@total';
                $t_priceCheck = array();
                if (isset($response->HotelRoomResponse->rateCode)) {
                    $temp_array[0] = $response->HotelRoomResponse;
                    $response->HotelRoomResponse = $temp_array;
                }
                $lowest_price = array();
                if (!empty($response)) {
                    if(isset($response->HotelRoomResponse)){
                        foreach ($response->HotelRoomResponse as $key2 => $RoomResponse) {
                            $total = '@total';
                            $lowest_price[$key2] = $RoomResponse->RateInfos->RateInfo->ChargeableRateInfo->$total;
                        }
                        $min_array = array_keys($lowest_price, min($lowest_price));
                    
                    
                        $singleRate = '@nightlyRateTotal';
                        foreach ($response->HotelRoomResponse as $key1 => $value1) {
                            $current_hotel = json_decode(json_encode($value1), true);
                            $singleRate = '@nightlyRateTotal';
                            $singleRate = $current_hotel['RateInfos']['RateInfo']['ChargeableRateInfo'][$singleRate];
                            $taxes = 0;
                            if (isset($current_hotel['RateInfos']['RateInfo']['taxRate'])) {
                                $taxes = $current_hotel['RateInfos']['RateInfo']['taxRate'];
                            }
                            $currentRate = $this->getExpediaHotelPriceWithEroamMarkup($singleRate, $nights, $taxes);
                            $singleRate = '@nightlyRateTotal';
                            $singleRate = $selected_hotel['RoomRateDetailsList']['RoomRateDetails']['RateInfos']['RateInfo']['ChargeableRateInfo'][$singleRate];
                            $taxes = 0;
                            if (isset($selected_hotel['RoomRateDetailsList']['RoomRateDetails']['RateInfos']['RateInfo']['taxRate'])) {
                                $taxes = $selected_hotel['RoomRateDetailsList']['RoomRateDetails']['RateInfos']['RateInfo']['taxRate'];
                            }
                            $selectedRate = $this->getExpediaHotelPriceWithEroamMarkup($singleRate, $nights, $taxes);
                            if ($currentRate != $selectedRate && $key1 == $min_array[0]) {
                                $t_priceCheck['oldRate'] = $selectedRate;
                                $t_priceCheck['newRate'] = $currentRate;
                                $t_priceCheck['hotelName'] = $selected_hotel['name'];
                                $t_priceCheck['hotelId'] = $selected_hotel['hotelId'];
                                $t_priceCheck['rateCode'] = $rateCode;
                                $t_priceCheck['city'] = $value['city']['name'];
                                $priceCheck[$key] = $t_priceCheck;
                            }
                        }
                    }
                }
            }
        }
        return $priceCheck;
    }
    public function expediaBooking($hotelId, $arrivalDate, $departureDate, $rateKey, $roomTypeCode, $rateCode, $chargeableRate, $passengers_info, $bedtypes, $customerSessionId,$GUIDCode,$specialInformation,$rateType) {

        $num_of_rooms = session()->get('search_input') ['rooms'];
        $hotel_request_data = session()->get('search_input');
        $children = array();
        if (isset($hotel_request_data['child']) && !empty($hotel_request_data['child'])) {
            $children = $hotel_request_data['child'];
        }

        $expedia_request = '';
        if ($num_of_rooms == 1) {
            $expedia_request.= '&room1=' . session()->get('search_input') ['num_of_adults'][0];
            if (isset($children[0]) && !empty($children[0])) {
                $c_array = implode(',', $children[0]);
                $c_array = str_replace('0', '1', $c_array);
                //$expedia_request.= ',' . count($children[0]) . ',' . $c_array;
                $expedia_request.= ',' . $c_array;
            }

            $bedTypeStr = '';
            if(isset($bedtypes[0])){
                $bedTypeStr = '&room1BedTypeId=' . $bedtypes[0]['bedTypeId'];
            }

            $expedia_request.= '&room1FirstName=' . $passengers_info['passenger_first_name'][0] . '&room1LastName=' . $passengers_info['passenger_last_name'][0] . $bedTypeStr.'&room1SmokingPreference=NS';

        } else {
            $pass = 0;
            for ($i = 0;$i < $num_of_rooms;$i++) {
                $n = $i + 1;
                $totalPass = session()->get('search_input') ['num_of_adults'][$i];
                $expedia_request.= '&room' . $n . '=' . $totalPass;
                if (isset($children[$i]) && !empty($children[$i])) {
                    $c_array = implode(',', $children[$i]);
                    $c_array = str_replace('0', '1', $c_array);
                    //$expedia_request.= ',' . count($children[$i]) . ',' . $c_array;
                    $expedia_request.= ',' . $c_array;
                }

                $bedTypeStr = '';
                if(isset($bedtypes[$n-1])){
                    $bedTypeStr = '&room'.$n.'BedTypeId=' . $bedtypes[$n-1]['bedTypeId'];
                }

                $expedia_request.= '&room' . $n . 'FirstName=' . $passengers_info['passenger_first_name'][$pass] . '&room' . $n . 'LastName=' . $passengers_info['passenger_last_name'][$pass] . $bedTypeStr . '&room' . $n . 'SmokingPreference=NS';
                $pass ++; //= $totalPass;

            }
        }
        
        $expedia_request .= '&AffiliateConfirmationId='.$GUIDCode;

        if($specialInformation){
             $expedia_request .= '&specialInformation='.$specialInformation;
        }

        $hotel_detail_data = array();
        $hotel_detail_data['hotelId'] = $hotelId;
        $hotel_detail_data['arrivalDate'] = $arrivalDate;
        $hotel_detail_data['departureDate'] = $departureDate;
        $hotel_detail_data['rateKey'] = $rateKey;
        $hotel_detail_data['roomTypeCode'] = $roomTypeCode;
        $hotel_detail_data['rateCode'] = $rateCode;
        $hotel_detail_data['chargeableRate'] = $chargeableRate;
        $hotel_detail_data['customerSessionId'] = $customerSessionId;
        $hotel_detail_data['expedia_request'] = $expedia_request;
        $hotel_detail_data['customeruseragent'] = getBrowserName();
        $hotel_detail_data['rateType'] = $rateType;
        
        $response = http('post', 'expedia/get-booking', $hotel_detail_data);
        return $response;
    }
    public function hotelVoucher($itineraryId = '', $rooms = '', $invoiceNumber = '', $rate = 0, $taxes = 0, $email1 = '', $city = '', $country_code = '', $leg = 0) {
        if (isset($itineraryId)) {
            $hotel_detail_data = array();
            $hotel_detail_data['itineraryId'] = $itineraryId;
            $hotel_detail_data['rooms'] = $rooms;
            $hotel_detail_data['invoiceNumber'] = $invoiceNumber;
            $hotel_detail_data['rate'] = $rate;
            $hotel_detail_data['taxes'] = $taxes;
            $hotel_detail_data['city'] = $city;
            $hotel_detail_data['country_code'] = $country_code;
            $hotel_detail_data['leg'] = $leg;
            $hotel_detail_data['email1'] = $email1;
            $hotel_detail_data['customeruseragent'] = getBrowserName();

            $data = http('post', 'expedia/get-voucher', $hotel_detail_data);

            if (isset(session()->get( 'search' )['itinerary'][$data['leg']]['hotel']['room'])){
                $data['room'] = session()->get( 'search' )['itinerary'][$data['leg']]['hotel']['room'];
            }

            if (isset(session()->get( 'search' )['itinerary'][$data['leg']]['hotel']['RoomRateDetailsList'])){
                $RoomRateDetails = session()->get( 'search' )['itinerary'][$data['leg']]['hotel']['RoomRateDetailsList']['RoomRateDetails']; 
                $RoomRateDetails = json_decode(json_encode($RoomRateDetails),true);

                if (isset($RoomRateDetails['rateDescription'])){
                    $data['roomDescription'] = $RoomRateDetails['rateDescription'];
                }

                if (isset($RoomRateDetails['BedTypes'])){
                    $data['BedTypes'] = $RoomRateDetails['BedTypes'];
                }

                if (isset($RoomRateDetails['checkInInstructions']) && $RoomRateDetails['checkInInstructions'] != ''){
                    $data['checkInInstructions'] = $RoomRateDetails['checkInInstructions'];
                }

                if (isset($RoomRateDetails['specialCheckInInstructions']) && $RoomRateDetails['specialCheckInInstructions'] != ''){
                    $data['specialCheckInInstructions'] = $RoomRateDetails['specialCheckInInstructions'];
                }
            }

            //$path = public_path('uploads') . '/' . $city . '_HotelVoucher_' . $itineraryId . '.pdf';
            $path = 'vouchers/hotels/'.$city.'_HotelVoucher_'.$itineraryId.'.pdf';        
            $pdf = PDF::loadView('itinenary.hotel-voucher', compact('data'));
            //$pdf->save($path);
            //return $pdf->stream('HotelVoucher.pdf');
            \Storage::disk('s3')->put($path, $pdf->output(), 'public');
        }
    }

    public function getExpediaHotelPriceWithEroamMarkup($Rate,$nights,$taxes){
         $data = [];
         $response = http('get', 'expedia/get-percentage', $data);
         $Rate = round(($Rate * $response['eroamPercentage']) / 100 + $Rate,2); 
         $subTotal = $nights * $Rate;
         $Rate = $subTotal + $taxes;
         return $Rate;            
    }
}
