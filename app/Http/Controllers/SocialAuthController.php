<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Socialite;
use Session;

class SocialAuthController extends Controller
{
	private $headers = [];

	public function __construct() {
		$this->headers = [
			'X-Authorization' => '7b94b3635849f1da030358d5724c72c1f320ca5c',
			'Origin' => url('')
		];
		
	}

    public function redirect($service)
    {
    	return Socialite::driver($service)->redirect();   

    }   

    public function callback($service)
    {
        $user = Socialite::with( $service )->user();
		$url = request()->root().'/profile/step1';

		if(session()->has('page')) {
			$url = session()->get('page');
		}

		if(!empty($user))
		{
			$data = array(
				'first_name' 			=> $user->user['name']['givenName'],
				'last_name'  			=> $user->user['name']['familyName'],
				'social' 				=> $service,	
				'email'		 			=> $user->email,
				'social_id'				=> $user->id,
				'image_path' 			=> $user->avatar_original
			);

			$check_user = http( 'post', 'user/check-customer', $data, $this->headers);

			if( $check_user['successful'] == 1 )
		    {
		    	if( isset($check_user['code']) )
		    	{
		    		$user_code = $check_user['code'];
					session()->put('user_auth', $user_code);
					if( session()->get('tour_book_user') )
					{
						dd('innn');
						$data 		= session()->get('tour_book_user');	
						$id 		= session()->get('user_auth')['id'];						
						$headers 	= 	[
											'X-Authorization' => '7b94b3635849f1da030358d5724c72c1f320ca5c',
											'Origin' => url(''),
										];
						$user 		= http('get', 'user/get-by-id/'.$id, [], $headers);
						$user 		= json_decode(json_encode($user), false);					
						$all_countries = get_all_countries();

						if(session()->get('user_auth')['id'])
						{
							$first_name         = $user->customer->first_name;
							$last_name          = $user->customer->last_name;
							$email              = $user->customer->email;
							$contact_no         = $user->customer->contact_no;
							$bill_address_2     = $user->customer->bill_address_1;
							$bill_address_1     = $user->customer->bill_address_2;
							$bill_country       = $user->customer->bill_country;
							$user_id            = $user->id;
						}

						$data_arr 	= [			
								'first_name' 	=> $first_name,
								'last_name' 	=> $last_name,
								'email' 		=> $email,
								'contact_no' 	=> $contact_no,
								'bill_address_1'=> $bill_address_1,
								'bill_address_2'=> $bill_address_2,
								'bill_country' 	=> $bill_country,
								'user_id' 		=> $user_id,
							];		
						
						$payError = 0; 
						if(Session::has('error'))
						{
						  	$payError = 1;
						}
						$is_DateRadioIdValue = 0;

						if(session()->has('DateRadioIdValue'))
						{ 
						  	$is_DateRadioIdValue = session()->get('DateRadioIdValue');
						}
						//session()->forget('tour_book_user');
						return view('pages.tour_booking')->with(compact('data','user','all_countries','data_arr','payError','is_DateRadioIdValue','days'));
					}
					
					// Session::flash('success', 'Successfully updated');
					return redirect($url);
				}

		        $response_api = http( 'post', 'user/create_customer', $data, $this->headers);
		        if( $response_api['successful'] )
		        {        			        	
					$user_code = $response_api['code'];
					session()->put('user_auth', $user_code);
					Session::flash('profile_step1_success', 'Thank you for registration!');
					return redirect($url)
		                    ->with('profile_step1_success', 'Thank you for registration!'); 
				}
		        else
		        {
					Session::flash('register_confirm_fail', 'Something went wrong!'); 
		            return redirect('/')
                    ->with('register_confirm_fail', 'Something went wrong!');
				}	
	        }
	        else
	        {
				Session::flash('register_confirm_fail', 'Email address already exist!');
				return redirect('/')
                    ->with('register_confirm_fail', 'Email address already exist!');
	        }
	    }
    }
}