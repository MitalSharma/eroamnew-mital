<?php

namespace App\Service\Mystifly;

/**
 * 
 */
class GetCityAirportCode
{
	

	 public function getCityAirportCode(){
        $pathHelper = resolve('PathHelper');
        $api_url = $pathHelper->getApiPath();
        $domain = $pathHelper->getDomain();
        
        $data = array();
        $request = TRUE;
        while( $request ){
                try{
                    $client = new \GuzzleHttp\Client(['headers' => ['domain' => $domain]]);
                    $response = $client->request('post',$api_url . 'get-city-airport-code', [
                        'form_params' => $data
                    ]);
                    $request = FALSE;
                }catch(Exception $e){
                Log::error('An error has occured during a guzzle call on MystiflyController@set for provider "'.$provider.'" '.'with key "'.$key.'" and ERROR MESSAGE:'.$e->getMessage() );
            }
        }
                
        $result = json_decode( $response->getBody() , true );
        
        return $result['data'];
        
    }
}