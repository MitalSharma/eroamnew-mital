var aot = ( function( $ ) {

	function clean(type, data, additional, other){

	    var result = {};

	    switch(type){

	        case 'sir_response':

	            result.code        = data.SupplierCode;
	            result.name        = data.Name;
	            result.description = data.Description;
	            result.rooms       = [];
	            result.currency    = 'AU$';
	            result.category    = parseInt(Math.round(data.StarRating.replace(/[^0-9.]/g, '')));
	            result.lat         = data.MetaData.Latitude;
	            result.lng         = data.MetaData.Longitude;
	            result.iata        = additional.iatacode;
	            result.type        = additional.api_type;
	            result.image       = (data.images) ? ($.isArray(data.images.image)) ? data.images.image[0].ServerPath : data.images.image.ServerPath : '/images/default_images/hotel/standard.jpg';
	            result.other       = {location_id : data.LocationID};
	            break;

	        case 'orr_response':

	            var room_rates = data.Rates.OptRate;
	            var room_price = {
	                single : 0,
	                double : 0,
	                twin : 0,
	                triple : 0,
	                quad : 0,
	                count : 0
	            };
	            var room_code = data.Opt;

	            if(room_rates[0].RoomRates){

	                var temp = room_rates.map(function(val){

	                    var count = 0;
	                    if(val.RoomRates.SingleRate){
	                        room_price.single += parseFloat(val.RoomRates.SingleRate);
	                        count++;
	                    }

	                    if(val.RoomRates.DoubleRate){
	                        room_price.double += parseFloat(val.RoomRates.DoubleRate);
	                        count++;
	                    }

	                    if(val.RoomRates.TwinRate){
	                        room_price.twin += parseFloat(val.RoomRates.TwinRate);
	                        count++;
	                    }

	                    if(val.RoomRates.TripleRate){
	                        room_price.triple += parseFloat(val.RoomRates.TripleRate);
	                        count++;
	                    }

	                    if(val.RoomRates.QuadRate){
	                        room_price.quad += parseFloat(val.RoomRates.QuadRate);
	                        count++;
	                    }

	                    room_price.count = count;

	                });

	            }

	            result.code = room_code;
	            result.price = room_price;
	            result.order = room_price.count;
	            break;

	        case 'orr_filter':

	            var max = 0;
	            result = false;

	            max = Math.max.apply(Math, data.map(function(value) {
	                return value.order;
	            }));

	            if(max > 0){

	                var rates_order = data.filter(function (value, key) {
	                    return value.order === max;
	                });

	                if($.isArray(rates_order)){

	                    var rates_filtered = rates_order.reduce(function(prev, next){
	                        var filter = false;
	                        var temp = false;
	                        var room = additional;

	                        if(prev.price[room] > 0 && next.price[room] > 0){

	                            temp = room;

	                        }else{

	                            if(next.price.single > 0){

	                                temp = 'single';

	                            }

	                            if(next.price.double > 0){

	                                temp = 'double';

	                            }

	                            if(next.price.twin > 0){

	                                temp = 'twin';

	                            }

	                            if(next.price.triple > 0){

	                                temp = 'triple';

	                            }

	                            if(next.price.quad > 0){

	                                temp = 'quad';

	                            }

	                        }

	                        if(prev.price[temp] < next.price[temp]){

	                            filter = prev;

	                        }else{

	                            filter = next;

	                        }

	                        return filter;

	                    });

	                    result = rates_filtered;

	                }

	            }

	            break;

	        case 'q_supp_response':
	            var metedata = $.parseJSON(data.MetaData),
	                images = $.parseJSON(data.Images);

	            result.code        = data.SupplierCode;
	            result.name        = data.Name;
	            result.description = data.Description;
	            result.rooms       = clean('q_supp_rooms', data, additional.room_type);
	            result.room_codes  =  [];
	            if( result.rooms.length > 0){
	            	$.each( result.rooms, function( index, value ){
	            		result.room_codes.push( value.code );
	            	} )
	            }
	            result.currency    = 'AU$';
	            result.category    = parseInt(Math.round(data.StarRating.replace(/[^0-9.]/g, '')));
	            result.lat         = metedata.Latitude;
	            result.lng         = metedata.Longitude;
	            result.iata        = additional.iatacode;
	            result.type        = additional.api_type;
	            result.image       = (images && images.image) 
	            ? ($.isArray(images.image)) ? images.image[0].ServerPath : images.image.ServerPath
	            : '/images/default_images/hotel/standard.jpg';
	            result.other = {location_id : data.LocationID};
	            result.location    = data.Address1 + ' ' + data.Address2;
	            break;

	        case 'q_supp_rooms':

	            var array = [];

	            for (var i = 0; i < data.rooms.length; i++) {

	                var temp = {
	                    code : data.rooms[i].Opt,
	                    order : 0
	                };

	                if(data.rooms[i].Single_Avail == 'Y'){
	                    temp.order++;
	                }

	                if(data.rooms[i].Double_Avail == 'Y'){
	                    temp.order++;
	                }

	                if(data.rooms[i].Twin_Avail == 'Y'){
	                    temp.order++;
	                }

	                if(data.rooms[i].Triple_Avail == 'Y'){
	                    temp.order++;
	                }

	                if(data.rooms[i].Quad_Avail == 'Y'){
	                    temp.order++;
	                }

	                array.push(temp);

	            }

	            var max = 0;

	            max = Math.max.apply(Math, array.map(function(value) {
	                return value.order;
	            }));

	            if(max > 0){

	                result = array.filter(function (value, key) {
	                    return value.order === max;
	                });

	            }

	            break;

	        case 'q_supp_rooms_merge':

	            var array = [];

	            for (var i = 0; i < data.length; i++) {
	                
	                for (var j = 0; j < data[i].rooms.length; j++) {
	                    
	                    array.push(data[i].rooms[j].code);

	                }

	            }

	            result = array;

	            break;

	        case 'supp_rooms_merge':

	            var temp = false,
	            result = [];

	            for (var i = 0; i < data.length; i++) {

	                for (var j = 0; j < additional.array.length; j++) {

	                    if(additional.array[j].code.indexOf(data[i].code) !== -1){

	                        data[i].rooms.push(additional.array[j]);

	                    }

	                }

	            }

	            data = data.filter(function(val){
	                return val.rooms.length > 0
	            });

	            for (var i = 0; i < data.length; i++) {

	                temp = clean('orr_filter', data[i].rooms, additional.room_type);
	                result.push(temp);

	            }

	            data = data.map(function(val){
	                val.rooms = [];
	                return val;
	            });

	            break;

	    }

	    return result;

	}

	function getCodeArray(array, code, type){

	    var result, found;

	    if(type == 'key'){
	        result = false;
	    }else if(type == 'count'){
	        result = 0;
	    }else if(type == 'data'){
	        result = null;
	    }

	    found = array.some(function (value, key) {

	        if(value && value.code.indexOf(code) !== -1){

	            if(type == 'count'){
	                result++;
	            }else if(type == 'key'){
	                result = key;
	            }else if(type == 'data'){
	                value.index = key;
	                result = value;
	            }

	        }

	    });

	    return result;

	}

	function separateRoomTypesPrices( aotHotelsArray, aotHotelsArrayMerged ){
		if( aotHotelsArray.length < 1 ) return false;
		for (var i = 0; i < aotHotelsArray.length; i++) {
			
			var room_rates = getCodeArray(aotHotelsArrayMerged, aotHotelsArray[i].code, 'data');

			if(room_rates){

				if(room_rates.price.single > 0){

					aotHotelsArray[i].rooms.push({
						name : 'single',
						code : room_rates.code,
						price : room_rates.price['single']
					});

				}

				if(room_rates.price.double > 0){

					aotHotelsArray[i].rooms.push({
						name : 'double',
						code : room_rates.code,
						price : parseFloat(room_rates.price['double'] / 2)
					});

				}

				if(room_rates.price.twin > 0){

					aotHotelsArray[i].rooms.push({
						name : 'twin',
						code : room_rates.code,
						price : parseFloat(room_rates.price['twin'] / 2)
					});

				}

				if(room_rates.price.triple > 0){

					aotHotelsArray[i].rooms.push({
						name : 'triple',
						code : room_rates.code,
						price : parseFloat(room_rates.price['triple'] / 3)
					});

				}

				if(room_rates.price.quad > 0){

					aotHotelsArray[i].rooms.push({
						name : 'quad',
						code : room_rates.code,
						price : parseFloat( room_rates.price['quad'] / 4 )
					});

				}

				aotHotels[aotHotelsArray[i].code] = aotHotelsArray[i];
			}

		}

		return aotHotelsArray;

	}
	return {
		clean: clean,
		getCodeArray: getCodeArray,
		separateRoomTypesPrices : separateRoomTypesPrices,
	};

})( jQuery );