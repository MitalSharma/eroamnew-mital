<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php
echo $data['city'] . '_ActivityVoucher_' . $data['bookingId'] . '.pdf';
?></title>
    <style type="text/css">
      @page {
        margin: 15px;
      }
      body{
        font-family: Arial;
        color: #212121;
        font-size: 14px;
        margin: 0px;
      }
      p{
        margin: 5px 0;
      }
      ul li{ padding: 4px 0; }
      h2{margin: 5px 0;}
      table tr th{margin-bottom: 30px;}
    </style>
  </head>
  <body>
    <?php
$logo = getDomainLogo();
if (!$logo) {
    $logo = public_path('/images/logo-big.png');
}
?>
   <table width="100%" cellpadding="4">
      <tbody>
        <tr>
          <td colspan="2">
            <table width="100%" style="background-color: #212121; color: #fff;">
              <tr>
                <td style="padding: 0 10px;">
                  <img src="{{ $logo }}" alt="eRoam" style="width: 100px;">
                </td>
                <td style="text-align: right; padding: 0px 15px; vertical-align: middle;">
                  <h2 style="margin-top: 4px;"><img src="{{ public_path('/images/ic_local_hotel.svg') }}" alt="" style="position: relative; top: 3px; margin-top: 4px;"/> Activity Voucher</h2>
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td colspan="2">
            <table width="100%" style="border: solid 3px #fafafa;text-align: left; margin-top: 5px;" cellspacing="4" cellpadding="6">
              <thead>
                <tr style="background-color: #fafafa;">
                  <th colspan="3">
                    <h2 style="margin: 8px 8px 8px 2px;">Booking Details</h2>
                  </th>
                </tr>
              </thead>
              <tbody>
                 
                <tr>
                    <td>
                        <p><strong>Name:</strong> {{$pass_info['billing_first_name']}} {{$pass_info['billing_last_name']}}</p>                    
                        <p><strong>Email:</strong> {{$pass_info['billing_email']}}</p>
                    </td>
                
                    <td>
                        <p><strong>Booking Id:</strong> {{$pass_info['booking_id']}}</p>
                        <p><strong>Booking Date:</strong> {{date("j M Y")}}</p>
                    </td>
                </tr>
                <tr>
                    <td>
                        <h3 style="margin: 0;">{{$actData['name']}}</h3>
                    </td>                                 
                </tr>     
                <tr>  
                    <td>
                        <p><strong>Activity Booking Date:</strong>{{date("j M Y",strtotime(@$actData['date_selected']))}}</p>
                        <p><strong>City Name:</strong> {{$data['city']}}</p>
                    </td>    
                    <td>
                        <?php 
                        $child = '';
                        if(isset($pass_info['child_first_name']))
                        {
                          if(count($pass_info['child_first_name']) > 0)
                          {
                              $child = count($pass_info['child_first_name'])." Children ";
                          }
                        }
                        $adult = count($pass_info['passenger_first_name']). " Adult ";
                        $passInfo = $adult.$child;
                        ?>
                        <p><strong>Travellers:</strong> {{$passInfo}}
                        <p><strong>Total Amount:</strong> 
                        AUD {{ number_format(@$actData['price'][0]['price'],2)}}</p>
                    </td>
                </tr>                                         
              </tbody>
            </table>
          </td>
        </tr>        
      </tbody>
    </table>
  </body>
</html>