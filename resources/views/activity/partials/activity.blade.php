<style type="text/css">
    .slick-slide{
        height: auto !important;
    }
</style>
<div class="itinerary_right">
    @include('itinenary.partials.sidebarstrip')
    <div class="container-fluid">
        <div id="view-more-details"></div>
        <div class="all-activity-list">
            <div class="outerpage_scroll itinerary_filter transport_filter pt-4 pb-3 ">
                <div class="pl-3">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="pb-3 mb-0 lh-125">
                                <div class="media">
                                    <i class=" ic-place location fa-lg pt-1"></i>
                                    <div class="media-body pb-3 mb-0">
                                        <h5>
                                            <strong class="d-block text-gray-dark"> {{ $city->name }}, {{ $city->country_name }}<span class="act-count"></span></strong>
                                        </h5>
                                        <span class="d-block"><i class="ic-calendar"></i>{{ convert_date($date_from, 'new_eroam_format') }} - {{ convert_date($date_to, 'new_eroam_format') }}</span>
                                    </div>
                                </div>
                            </div>                        
                        </div>

                        <div class="col-sm-6">
                            <div class="form-group">
                                <div class="fildes_outer">
    								<label>@lang('home.search_by_activity_name')</label>
    								<input type="text" id="search_val" class="form-control" aria-describedby="emailHelp" placeholder="Search">
                                    <span class="arrow_down" onclick="getActivitySearch();"><i class="fa fa-search"></i> </span>
    						    </div>
    				       </div>
                        </div>
                    </div>
                    <?php 
                    $date_from = strtotime($date_from. ' +1 day'); // Convert date to a UNIX timestamp  
                    $date_to = strtotime($date_to. ' -1 day');

                    ?>
                    <section class="regular slider mt-2">
                        @php 
                        for ($i=$date_from; $i<=$date_to; $i+=86400) 
                        {      
                            $active_class = '';          
                            if($date_from_search == date("Y-m-d", $i))    
                            {
                                $active_class = "active";                            
                            }          
                            $date = date("Y-m-d", $i);
                            $date_search = strtotime($date);          
                        @endphp
                        <li class="{{$active_class}}"> <!--active class add-->
                            <a href="{{url($city->name.'/activities?leg='.$leg.'&dt='.$date_search)}}"><?php echo date("d M", $i); ?></a>
                        </li>
                        @php
                        }
                        @endphp      
                    </section>
                    <div class="filter_poperties border-bottom pb-1 mt-4 mb-3">
                        <div class="row">
                            <div class="col-12 col-sm-12 col-md-10 col-lg-12 col-xl-10">
                                <span class="d-inline-block pt-2">Filter Properties By:</span>
                                <div class="poperties ">
                                    <ul class="nav nav-pills nav-fill activities_nav">
                                        <li class="nav-item">
                                            <a data-toggle="tooltip" title="@lang('home.pilot_text')" class="nav-link disabled" href="#">@lang('home.top_picks')  <i class="fa fa-caret-down" aria-hidden="true"></i> </a>
                                        </li>

                                        <li role="presentation" class="nav-item"> <a href="#" class="nav-link" id="ratingAsc">@lang('home.rating') <i class="fa fa-caret-down" aria-hidden="true"></i></a>
                                        </li>

                                        <li role="presentation" class="nav-item"> <a href="#" class="nav-link" id="priceSortAsc">@lang('home.lowest_price') <i class="fa fa-caret-down" aria-hidden="true"></i> </a>
                                        </li>


                                        <li class="nav-item">
                                            <a class="nav-link dropdown-toggle " data-toggle="dropdown" href="javascript:" role="button" aria-haspopup="true" aria-expanded="false" href="#">@lang('home.category') </a>
                                            <ul class="dropdown-menu border-0 rounded-0" id="category-tab">
                                                <li><a href="#" class="dropdown-item" id="searchCategory">@lang('home.all_text')</a></li>
                                            </ul>
                                        </li>

                                        <!-- <li class="nav-item">
                                            <a class="nav-link disabled" href="#" data-toggle="tooltip" title="@lang('home.pilot_text')">@lang('home.stops') <i class="fa fa-caret-up" aria-hidden="true"></i> </a>
                                        </li> -->
                                    </ul>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-2 col-lg-12 col-xl-2">
                                <div class="text-right filtericons text-lg-left text-xl-right">
                                    <a class="active" id="listview-tab" href="javascript:void(0)"><i class="ic-menu"></i></a>
                                    <a class="" id="gridview-tab"  href="javascript:void(0)"><i class="ic-apps"></i></a>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="itinerary_page pt-2 tabs-content-container">
                <div class="tab-content activity-wrapper" id="myTabContent">
                    <div class="in active" role="tabpanel" id="top-picks" aria-labelledby="top-picks-tab">
                        <div class="container-fluid activityScroll tabs-content-wrapper p-relative" id="activities-container">
                            <!-- Calendar Data Start -->
                            @php 
    						$i=0; 
    						$actArr = array(); 
    						foreach ($selected_dates as $name => $date) { 
    							$sdate = explode(' ', $date);
    							$sdate = date('Y',strtotime($sdate[2])).'-'.date('m',strtotime($sdate[1])).'-'.str_replace(array('th','rd','nd','st',','), '', $sdate[0]); 
    							$sdate2 = date('j M Y',strtotime($sdate)); 
    							$sdate = date('Y-m-d',strtotime($sdate)); 
    							//$actArr[$sdate] = session()->get('search')['itinerary'][$leg]['activities'][$i]['name']; 
    							$i++; 
    						} 
    						@endphp

                            <div id="activity-loader" class="loader_container">
                                <div><i class="fa fa-circle-o-notch fa-spin"></i> @lang('home.loading_text')</div>
                            </div>
                            <div id="auto-sort-loader" class="loader_container" style="display:none;">
                                <div><i class="fa fa-circle-o-notch fa-spin"></i> @lang('home.sorting_text')</div>
                            </div>
                            <div class="grid-activity-list-selected activities row" style="display:none"></div>
                            <div class="activity-list-selected activities"></div>
                            <div class="tab-pane fade  show active" role="tabpanel" id="listview" aria-labelledby="listview-tab">                            
                                <div class="activities activity-list" id="activity-list">
                                    <h4 class="text-center blue-txt bold-txt no_activities" style="display:none;">No Activities Found.</h4>
                                </div>
                            </div>

                            <div class="tab-pane fade" id="gridview" aria-labelledby="gridview-tab" role="tabpanel" style="display:none">    
                                <h4 class="text-center blue-txt bold-txt no_activities_grid" style="display:none;">No Activities Found.</h4>                       
                                <div class="activities grid-activity-list row" id="grid-activity-list">
                                    
                                </div>
                            </div> 

                        </div>
                        
                                          
    					
    					<div class="p-relative">
    						<div id="view-more-loader" class="loader_container" style="display:none;position: inherit;">
    							<div><i class="fa fa-circle-o-notch fa-spin"></i> @lang('home.loading_data_text')</div>
    						</div>
    						<!-- Activity Details Start -->
    						
    						<!-- Activity Details End -->
    						<!-- <div class="clearfix"></div> -->
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<input type="hidden" id="selected-activities" value="{{$selected_activities}}" />
<input type="hidden" class="the-city-id" value="{{ json_encode( get_city_by_id( $city_id ) ) }}">