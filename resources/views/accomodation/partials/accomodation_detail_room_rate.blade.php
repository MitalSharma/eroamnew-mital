@php 
	$displaySymbol = '';
	$disabled_status = '';
@endphp
@if($data->provider=="expedia")
	@if(isset($data->HotelRoomResponse))
		@foreach($data->HotelRoomResponse as $key => $RoomResponse)
			@php
				if(isset($data->checkInInstructions)){
					$RoomResponse->checkInInstructions = $data->checkInInstructions;
				}

				if(isset($data->specialCheckInInstructions)){
					$RoomResponse->specialCheckInInstructions = $data->specialCheckInInstructions;
				}

				$singleRate = '@nightlyRateTotal';
				$singleRate = $RoomResponse->RateInfos->RateInfo->ChargeableRateInfo->$singleRate;
				$singleRate = round($singleRate,2); 
				$taxes = $RoomResponse->RateInfos->RateInfo->taxRate;
				$total = $singleRate + $taxes;
				

				if($rateCode == $RoomResponse->rateCode){
					$displaySymbol = '+';
					$differencePrice =0;
					$disabled_status = 'disabled';
				}else{
					$disabled_status = '';
					$differencePrice = abs($total - $selectedPrice);
					if($total < $selectedPrice){
						$displaySymbol ='-';
					}else{
						$displaySymbol = '+';
					}
				}

				$currencyCode = '@currencyCode';
				$currencyCode = $RoomResponse->RateInfos->RateInfo->ChargeableRateInfo->$currencyCode;
			@endphp
			<div class="card rounded-0 border-0 mt-3">
				<div class="card-header border-0 rounded-0 " id="headingTwo">
					<div data-toggle="collapse" data-target="#collapseTwohotel{{$key}}" aria-expanded="false" aria-controls="collapseTwohotel{{$key}}">
						<div class="row">
							<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-8">
			                    <!-- <div class="thumb">
			                    <img src="images/room_thumb.png" alt="" />
			                </div> -->
			                <div class="roomes">
			                	<strong class="d-inline-block pb-1">{{$RoomResponse->rateDescription}}</strong>
			                	<p class="mb-0">Room sleeps {{$RoomResponse->rateOccupancyPerRoom}} Guests. {{($RoomResponse->RateInfos->RateInfo->nonRefundable == 0 ? 'Refundable':'Non-Refundable')}}</p>
			                </div>
			            </div>
			            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-4">
			            	@php
			            	$nights = 0;
			            	@endphp
			            	@if (session()->get('search'))
			            	@php
			            	$nights = session()->get('search')['itinerary'][$data->leg]['city']['default_nights'];
			            	@endphp
			            	@endif
			            	<div class="text-right pricebox">
			            		<div class="price">$AUD {{number_format($total / $travellers,2)}}</div>
			            		<p>Per Person For {{$nights}} {{($nights > 1 ? 'Nights':'Night')}}</p>
			            	</div>
			            </div>
			        </div>
			    </div>
			</div>
			<div id="collapseTwohotel{{$key}}" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
				<div class="card-body mb-2">
					<div class="row">
						<div class="col-12 col-sm-12 col-md-6 col-lg-12 col-xl-6">
							@php
								$nights = 0;
							@endphp
							@if (session()->get('search'))
							@php
							$nights = session()->get('search')['itinerary'][$data->leg]['city']['default_nights'];
							@endphp
							@endif
							<ul class="deluxe_room">
								<li><strong class="d-inline-block">@lang('home.check_in') :</strong> {{date('d M Y',strtotime($data->arrivalDate))}}</li>
								<li><strong class="d-inline-block">@lang('home.check_out') :</strong> {{date('d M Y',strtotime($data->departureDate))}} </li>
								<li><strong class="d-inline-block">@lang('home.duration_text') :</strong> {{$nights}} {{($nights > 1 ? 'Night(s)':'Night')}} </li>
								@isset($RoomResponse->BedTypes)
								<li><strong class="d-inline-block">@lang('home.Bed_Preferences') :</strong> 
									<?php 
										if($RoomResponse->BedTypes->$size > 1){
											$i =0; $firstBedType = ''; 
											foreach($RoomResponse->BedTypes->BedType as $bedType){
												$i++; $or ='';
												if($i == 1) { $firstBedType =  $bedType->$BedTypeId; }
												if($i < $RoomResponse->BedTypes->$size) { $or = ' <b>OR</b> ';}
												echo ucwords($bedType->description).$or;
										 	} 
										} else {
											//echo ucwords($RoomResponse->BedTypes->BedType->description);
											echo ucwords(findString($RoomResponse->RoomType->descriptionLong,'<strong>','</strong>'));
										}
									?>
								</li>
								@endisset
								</ul>

								@if(!empty($RoomResponse->RoomType->descriptionLong))
								<p class="mt-3">{!!html_entity_decode($RoomResponse->RoomType->descriptionLong)!!}</p>
								<p class="mt-3">@lang('home.room_sleep_text')  {{$RoomResponse->rateOccupancyPerRoom}} @lang('home.guests_text') .<br/>{{($RoomResponse->RateInfos->RateInfo->nonRefundable == 0 ? 'Refundable':'Non-Refundable')}}
								</p>
								@endif


								<span class="cancellation_policy"><a href="javascript://">@lang('home.View_Cancellation_Policy')</a>
									<div class="cancellation_policybox"> 
										<p>{{$RoomResponse->RateInfos->RateInfo->cancellationPolicy}}</p> 
									</div>
								</span>
							</div>
							<div class="col-12 col-sm-12 col-md-1 col-lg-1 col-xl-1"></div>
							<div class="col-12 col-sm-12 col-md-5 col-lg-5 col-xl-5">
								<table class="table borderless">
									<thead>
										<tr>
											<th valign="top" scope="col" class="text-left"><strong> @lang('home.guests_text') :</strong> </th>
											<th  scope="col" class="text-right"><strong> 
												{{$travellers}} x @lang('home.adult_text_label') 
												@if($total_childs > 0)
												&nbsp;&nbsp;&nbsp;<br>{{$total_childs}} x @lang('home.Child')
													<span data-toggle="tooltip" class="childTooltip" data-placement="top" title="" data-original-title="{{$childrenAge}}"><i class="fa fa-info-circle"></i></span>
												@endif
											</strong></th>
										</tr>
									</thead>
									<tbody>
										@php
										$NightlyRatesPerRoom = json_decode(json_encode($RoomResponse),true);
										if(isset($NightlyRatesPerRoom['RateInfos']['RateInfo']['RoomGroup']['Room'])){
											if(isset($NightlyRatesPerRoom['RateInfos']['RateInfo']['RoomGroup']['Room'][0])){
												$temp_rate = $NightlyRatesPerRoom['RateInfos']['RateInfo']['RoomGroup']['Room'];
											}else{
												$temp_rate[0] = $NightlyRatesPerRoom['RateInfos']['RateInfo']['RoomGroup']['Room'];
											}
											foreach($temp_rate as $chargeKey => $charges){
												$rate = '@rate';
												$chargesRate = 0;
												if(isset($charges['ChargeableNightlyRates'][0])){
													foreach($charges['ChargeableNightlyRates'] as $c){
														$chargesRate = $chargesRate + $c[$rate];
													}
												}else{
													$chargesRate = $charges['ChargeableNightlyRates'][$rate];
												}

												//$chargesRate = round(($chargesRate * $eroamPercentage) / 100 + $chargesRate,2);
												$chargesRate = number_format($chargesRate,2,'.','');
												@endphp
												<tr>
													<td class="text-left"><strong>@lang('home.room_text_label') - {{$chargeKey+1}}:</strong></td>
													<td class="text-right">$AUD <span class="">{{$chargesRate}}</span></td>
												</tr>
												@php 
											}
										}
										@endphp
										<tr>
											<td class="text-left"><strong> @lang('home.sub_total_text'):</strong></td>
											<td class="text-right">$AUD <span class="">{{number_format($total / $travellers,2)}}</span>
											</td>
										</tr>
										@php
										$sur_charges = json_decode(json_encode($RoomResponse),true);
										if(isset($sur_charges['RateInfos']['RateInfo']['ChargeableRateInfo']['Surcharges'])){
											$type = '@type';
											$amount = '@amount';
											$size = '@size';

											$temp_surcharge = $sur_charges['RateInfos']['RateInfo']['ChargeableRateInfo']['Surcharges']['Surcharge'];
											if(!isset($temp_surcharge[0])){
												$tempSurcharge[0] = $temp_surcharge;
												unset($temp_surcharge);
												$temp_surcharge = $tempSurcharge;
											}
											foreach($temp_surcharge as $charges){
												@endphp
												<tr>
													<td class="text-left"><strong>{{(isset($surcharge_components[$charges[$type]]) ? $surcharge_components[$charges[$type]]:'')}}:</strong></td>
													<td class="text-right">$AUD <span class="">{{$charges[$amount]}}</span></td>
												</tr>
												@php 
											}
										}else{
											@endphp
											<tr>
												<td class="text-left"><strong>@lang('home.Taxes'):</strong></td>
												<td class="text-right">$AUD <span class="">{{$taxes}}</span></td>
											</tr> 
											@php
										}
										@endphp
									</tbody>
								</table>
								<div class="price_outer">
									<div class="price text-right">
										$AUD {{number_format($total / $travellers,2)}}
									</div>
									<p class="text-right pt-3">{{$nights}} {{($nights > 1 ? 'Nights':'Night')}}<br/>Rates are quoted in Australian Dollars.</p>
									@isset($sur_charges['RateInfos']['RateInfo']['HotelFees']['HotelFee']['@amount'])
									<table class="table borderless">
										<tbody>
											<tr>
												<td scope="col" class="text-left">
													<strong> @lang('home.Mandatory_fees_hotel') <span data-toggle="tooltip" data-placement="top" title="" data-original-title="@lang('home.hotelFeesIndo',['price'=>'$AUD '.$sur_charges['RateInfos']['RateInfo']['HotelFees']['HotelFee']['@amount']])"><i class="fa fa-info-circle"></i></span> :</strong> 
												</td>
												<td scope="col" class="text-right">$AUD {{$sur_charges['RateInfos']['RateInfo']['HotelFees']['HotelFee']['@amount']}}</strong></td>
											</tr>
										</tbody>
									</table>
									@endisset
								</div>

								<button {{$disabled_status}} type="button" name="" class="btn btn-secondary active btn-block m-t-20 selected-room" data-bedtype="<?php if(isset($firstBedType)){ echo $firstBedType; } ?>" data-provider="{{$data->provider}}" data-hotel="{{$data->hotelId}}" data-room="{{json_encode($RoomResponse)}}" >@lang('home.add_to_itinerary_link')</button>
							</div>
						</div>
						<div class="row">
							<div class="col-12 col-sm-12 col-md-6 col-lg-12 col-xl-8">

								
								<strong><a href="#" data-toggle="modal" data-target="#roomTypeModal{{$key}}" class="more-info moreInfo"><i class="fa fa-info-circle"></i> @lang('home.more_detail_text')</a></strong>
							
							</div>
						</div>
						<div class="modal fade" class="roomTypeModal" id="roomTypeModal{{$key}}" tabindex="-1" role="dialog">
							<div class="modal-dialog modal-md" role="document">
								<div class="modal-content">
									<div class="connected-carousels">
										<div class="stage">
											<div class="carousel carousel-stage">
												<ul>
													@if($data->HotelImages->HotelImage)
													@foreach ($data->HotelImages->HotelImage as $key => $value)
													@if ($key <= 30)
													<li><img src="{{$value->url}}" alt="" width="600" height="400"></li>
													@endif
													@endforeach
													@endif
												</ul>
											</div>
										</div>
										<div class="navigation">
											<a href="#" class="prev prev-navigation">&lsaquo;</a>
											<a href="#" class="next next-navigation">&rsaquo;</a>
											<div class="carousel carousel-navigation">
												<ul>
													@if($data->HotelImages->HotelImage)
													@foreach ($data->HotelImages->HotelImage as $key => $value)
													@if ($key <= 30)
													<li><img src="{{$value->url}}" alt="" width="80" height="80"></li>
													@endif
													@endforeach
													@endif
												</ul>
											</div>
										</div>
									</div>
									<div class="modal-body">
										<div class="roomType-inner mt-5">
											<div class="mt-4">
												@if(!empty($RoomResponse->RoomType->descriptionLong))
												<p>{!!html_entity_decode($RoomResponse->RoomType->descriptionLong)!!}</p>
												@endif

											</div>
											<div class="m-t-30 text-right">
												<a href="#" data-dismiss="modal" class="modalLink-blue text-info">@lang('home.close_link_cap') </a>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		@endforeach
	@else
		<div class="card rounded-0 border-0 mt-3">
			<div class="card-header border-0 rounded-0 " id="headingTwo">
				<p><center><strong class="d-inline-block pb-1">@lang('home.No_Rooms')</strong></center></p>
			</div>
		</div>	
	@endif
@elseif($data->provider=="eroam")
	@if(isset($data->rooms))
		<?php 
		$aRooms = $data->rooms;
		
		?>
		@foreach($aRooms as $key => $aRoom)
		<div class="card rounded-0 border-0 mt-3">
			<div class="card-header border-0 rounded-0 " id="headingTwo">
				<div data-toggle="collapse" data-target="#collapseTwohotel{{$key}}" aria-expanded="false" aria-controls="collapseTwohotel{{$key}}">
					<div class="row">
						<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-8">
		                    <!-- <div class="thumb">
		                    <img src="images/room_thumb.png" alt="" />
		                </div> -->
		                <div class="roomes">
		                	<strong class="d-inline-block pb-1">{{$aRoom->name}}</strong>
		                	<p class="mb-0">Room sleeps {{$aRoom->max_pax}} Guests. {{ ($data->refundable == 1) ? 'Refundable':'Non-Refundable' }}</p>
		                </div>
		            </div>
		            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-4">
		            	@php
		            	$nights = 0;
		            	@endphp
		            	@if (session()->get('search'))
		            	@php
		            	$nights = session()->get('search')['itinerary'][$data->leg]['city']['default_nights'];
		            	@endphp
		            	@endif
		            	<div class="text-right pricebox">
		            		@php
		            			$roomPrice = $aRoom->price * $nights;
								$roomPrice = number_format($roomPrice,2);
		            		@endphp
		            		<div class="price">$AUD {{$roomPrice}}</div>
		            		<p>Per Person For {{$nights}} {{($nights > 1 ? 'Nights':'Night')}}</p>
		            	</div>
		            </div>
		        </div>
		    </div>
		</div>
		<div id="collapseTwohotel{{$key}}" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
			<div class="card-body mb-2">
				<div class="row">
					<div class="col-12 col-sm-12 col-md-6 col-lg-12 col-xl-6">
						@php
						$nights = 0;
						@endphp
						@if (session()->get('search'))
						@php
						$nights = session()->get('search')['itinerary'][$data->leg]['city']['default_nights'];
						@endphp
						@endif
						<ul class="deluxe_room">
							<li><strong class="d-inline-block">@lang('home.check_in') :</strong> {{date('d M Y',strtotime($data->arrivalDate))}}</li>
							<li><strong class="d-inline-block">@lang('home.check_out') :</strong> {{date('d M Y',strtotime($data->departureDate))}} </li>
							<li><strong class="d-inline-block">@lang('home.duration_text') :</strong> {{$nights}} {{($nights > 1 ? 'Night(s)':'Night')}} </li>
							</ul>
							
							@if(!empty($aRoom->description))
							<p class="mt-3">{!!html_entity_decode($aRoom->description)!!}</p>
							<p class="mt-3">@lang('home.room_sleep_text')  {{$aRoom['max_pax']}} @lang('home.guests_text') .<br/>{{ ($data['refundable'] == 1 ? 'Refundable':'Non-Refundable') }}
							</p>
							@endif
							
							@if($aRoom->cancellation_policy != '')
							<span class="cancellation_policy"><a href="javascript://">@lang('home.View_Cancellation_Policy')</a>
								<div class="cancellation_policybox"> 
									<p>{!! $aRoom->cancellation_policy !!}</p> 
								</div>
							</span>
							@endif
						</div>
						<div class="col-12 col-sm-12 col-md-1 col-lg-1 col-xl-1"></div>
						<div class="col-12 col-sm-12 col-md-5 col-lg-5 col-xl-5">
							<table class="table borderless">
								<thead>
									<tr>
										<th valign="top" scope="col" class="text-left"><strong> @lang('home.guests_text') :</strong> </th>
										<th  scope="col" class="text-right"><strong> 
											{{$travellers}} x @lang('home.adult_text_label') 
											@if($total_childs > 0)
											&nbsp;&nbsp;&nbsp;<br>{{$total_childs}} x @lang('home.Child')
											<span data-toggle="tooltip" class="childTooltip" data-placement="top" title="" data-original-title="{{$childrenAge}}"><i class="fa fa-info-circle"></i></span>
											@endif
										</strong></th>
									</tr>
								</thead>
								<tbody>
									@php

									$roomPrice = $aRoom->price * $nights;
									$nRooms = $data->num_of_rooms;
									$nSubTotal = 0;

									@endphp
									@for($room=1 ; $room <= $nRooms ; $room++ )
										@php $nSubTotal = (float)$nSubTotal+(float)$roomPrice; 
										@endphp
										<tr>
											<td class="text-left"><strong>@lang('home.room_text_label') - {{$room}}:</strong></td>
											<td class="text-right">${{ $data->currencyCode }}<span class="">{{number_format($roomPrice,2)}}</span></td>
										</tr>
									
									@endfor
									
									<tr>
										<td class="text-left"><strong> @lang('home.sub_total_text'):</strong></td>
										<td class="text-right">$AUD <span class="">{{ number_format($nSubTotal,2)}}</span>
										</td>
									</tr>

									<tr>
										<td class="text-left"><strong>@lang('home.Taxes'):</strong></td>
										<td class="text-right">$AUD <span class="">{{$taxes ?? '00.00'}}</span></td>
									</tr> 
								</tbody>
							</table>
							<div class="price_outer">
								<div class="price text-right">
									@php
										$roomPrice = $aRoom->price * $nights;
										$roomPrice = number_format($roomPrice,2);
									@endphp
									$AUD {{number_format($nSubTotal,2)}}
								</div>
								<p class="text-right pt-3">{{$nights}} {{($nights > 1 ? 'Nights':'Night')}}<br/>Rates are quoted in Australian Dollars.</p>
							</div>
							
							<button {{(($data->defaultKey != 'NO' && $key == 0) ? 'disabled': '')}} type="button" name="" class="btn btn-secondary active btn-block m-t-20 selected-room" data-bedtype="<?php if(isset($firstBedType)){ echo $firstBedType; } ?>" data-provider="{{$data->provider}}" data-hotel="{{$data->hotelId}}" data-room="{{json_encode($aRoom)}}" >@lang('home.add_to_itinerary_link')</button>
						</div>
					</div>
					<div class="row">
						<div class="col-12 col-sm-12 col-md-6 col-lg-12 col-xl-8">
							<?php if(!empty($data->HotelImages->HotelImage) || !empty($aRoom->description)) { ?>
							<strong><a href="#" data-toggle="modal" data-target="#roomTypeModal{{$key}}" class="more-info moreInfo"><i class="fa fa-info-circle"></i> @lang('home.more_detail_text')</a></strong>
						<?php } ?>
						</div>
					</div>
					<div class="modal fade" class="roomTypeModal" id="roomTypeModal{{$key}}" tabindex="-1" role="dialog">
						<div class="modal-dialog modal-md" role="document">
							<div class="modal-content">
								<div class="connected-carousels">
									<div class="stage">
										<div class="carousel carousel-stage">
											<ul>
												@if($data->HotelImages->HotelImage)
												@foreach ($data->HotelImages->HotelImage as $key => $value)
												@if ($key <= 30)
												<li><img src="{{$value->url}}" alt="" width="600" height="400"></li>
												@endif
												@endforeach
												@endif
											</ul>
										</div>
									</div>
									<div class="navigation">
										<a href="#" class="prev prev-navigation">&lsaquo;</a>
										<a href="#" class="next next-navigation">&rsaquo;</a>
										<div class="carousel carousel-navigation">
											<ul>
												@if($data->HotelImages->HotelImage)
												@foreach ($data->HotelImages->HotelImage as $key => $value)
												@if ($key <= 30)
												<li><img src="{{$value->url}}" alt="" width="80" height="80"></li>
												@endif
												@endforeach
												@endif
											</ul>
										</div>
									</div>
								</div>
								<div class="modal-body">
									<div class="roomType-inner mt-5">
										<div class="mt-4">
											@if(!empty($aRoom->description))
											<p>{!!html_entity_decode($aRoom->description)!!}</p>
											@endif
											
										</div>
										<div class="m-t-30 text-right">
											<a href="#" data-dismiss="modal" class="modalLink-blue text-info">@lang('home.close_link_cap') </a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		@endforeach
	@else
	<div class="card rounded-0 border-0 mt-3">
		<div class="card-header border-0 rounded-0 " id="headingTwo">
			<p><center><strong class="d-inline-block pb-1">@lang('home.No_Rooms')</strong></center></p>
		</div>
	</div>	
	@endif
@endif