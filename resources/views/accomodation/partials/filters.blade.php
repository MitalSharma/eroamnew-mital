<div class="itinerary_filter pt-3">
    <div class="row">
        <div class="col-12 col-sm-6 col-md-6 col-lg-6 col-xl-6">
            <div class="itinerary_titles pl-5 ml-2">
                <i class=" ic-place location "></i>
                <h5>{{$data['city_name']}}, {{$data['city']['country_name']}}</h5>
                <p class="hotel-count"></p>
            </div>
        </div>
        <div class="col-12 col-sm-6 col-md-6 col-lg-6 col-xl-6">
            <div class="form-group">
                <div class="fildes_outer">
                    <label>@lang('home.accommodation_placeholder_search') </label>
                    <input type="text" class="form-control" id="search_val" placeholder="@lang('home.search_text')" data-toggle="tooltip">
                	<span class="input-group-btn">
	                    <button class="btn btn-default" type="button" onclick="getHotelSearch();"><i class="fa fa-search"></i> </button>
	                </span>
                </div>
            </div>
        </div>
    </div>
    <div class="filter_poperties border-bottom ml-4 pb-1 mb-5">
        <div class="row">

            <div class="col-12 col-sm-12 col-md-10 col-lg-10 col-xl-10 ">

                <div class="row">

                    <div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3 ">
                        <span>@lang('home.filter_property_text') </span>
                    </div>


                    <div class="col-12 col-sm-12 col-md-9 col-lg-9 col-xl-9 ">
                        <div class="transport_filter accomodation_filter">
                             <div class="row">
                                <div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3 ">
                                   <ul class="nav nav-pills nav-fill">
                                        <li class="nav-item dropdown">
                                           <a class="nav-link" id="RatingAsc" href="#">@lang('home.map_filter_text2')<i class="fa fa-caret-down ml-2" aria-hidden="true"></i></a>
                                        </li>
                                       
                                    </ul>
                                </div>

                                <div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3 ">
                                      <ul class="nav nav-pills nav-fill">
                                        <li class="nav-item dropdown">
                                            <a class="nav-link dropdown-toggle " data-toggle="dropdown" href="#" role="button" aria-controls="myTabDrop1-contents">@lang('home.map_filter_text3') </a>
                                            <ul class="dropdown-menu border-0 rounded-0" id="property-tab">
                                                <li><a class="dropdown-item" id="searchProperty">@lang('home.all_text')</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>

                                <div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3 ">
                                    <ul class="nav nav-pills nav-fill">
                                        <li class="nav-item">
                                            <a class="nav-link" href="#" id="priceSortAsc"> @lang('home.map_filter_text5')<i class="fa fa-caret-down ml-2" aria-hidden="true"></i> </a>
                                        </li>
                                    </ul>
                                </div>

                                <div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3 "id="extra-tab" style="display: none;">
                                      <ul class="nav nav-pills nav-fill" >
                                        <li class="nav-item dropdown">
                                            <a class="nav-link dropdown-toggle " data-toggle="dropdown" href="#" role="button" aria-controls="myTabDrop1-contents">@lang('home.map_filter_text6') </a>
                                            <ul class="dropdown-menu border-0 rounded-0" >
                                                <li><a class="dropdown-item">@lang('home.Free_Cancellation')</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-12 col-sm-12 col-md-2 col-lg-2 col-xl-2 ">

                <div class="float-right filtericons">
                    <a href="" id="listView" class="active"><i class="icon icon-list"></i></a>
                    <a href="" id="gridView" class="ml-1"><i class="icon icon-grid"></i></a>
                </div>
            </div>
         </div>
    </div>
</div>