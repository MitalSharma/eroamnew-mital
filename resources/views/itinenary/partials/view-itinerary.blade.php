@extends('itinenary.booking-layout')
@section('booking-content')
<div class="account-right itinerary_right">
    @include('itinenary.partials.sidebarstrip')
    <div class="container-fluid">
        <div class="steps stepsouter p-4">
            <h5><i class="ic-explore"></i> View Itinerary</h5>
            <div class="text-right d-block d-lg-none"><a href="javascript://" class="navbtn menu-btn text-secondary"><i class="ic-menu"></i></a></div>
            <div class="row mt-4">
                <div class="form-group col-md-3 col-sm-6">
                    <div class="fildes_outer">
                        <label>Check-in Date:</label>
                        <input id="checkin-date" type="text" placeholder="{{$startDate}}" readonly class="form-control disabled valid" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Not Available in Pilot">
                        <span class="arrow_down"><i class="ic-calendar"></i> </span>
                    </div>
                </div>
                <div class="form-group col-md-3 col-sm-6">
                    <div class="fildes_outer">
                        <label>Check-out Date:</label>
                        <input id="checkout-date" type="text" placeholder="{{ $endDate }}" readonly class="form-control disabled valid" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Not Available in Pilot">
                        <span class="arrow_down"><i class="ic-calendar"></i> </span>
                    </div>
                </div>
                <div class="form-group col-md-2 col-sm-4">
                    <div class="fildes_outer">
                        <label>Rooms</label>
                        <div class="custom-select">
                            <select class="form-control disabled valid" name="rooms" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Not Available in Pilot">
                                <option value="{{$rooms}}">{{$rooms}} @if($rooms > 1) Rooms @else Room @endif</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-group col-md-2 col-sm-4">
                    <div class="fildes_outer">
                        <label>Adults (18+)</label>
                        <div class="custom-select">
                            <select class="form-control disabled valid" name="num_of_adults" data-toggle="tooltip" data-placement="bottom" title="Not Available in Pilot" data-original-title="Not Available in Pilot">
                                <option value="{{ $adults }}">{{ $adults }} @if($adults > 1) Adults @else Adult @endif</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-group col-md-2 col-sm-4">
                    <div class="fildes_outer">
                        <label>Children (0-17)</label>
                        <div class="custom-select">
                            <select class="form-control disabled valid" name="num_of_children[]" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Not Available in Pilot">
                                <option value="{{$children}}">@if($children != 0) {{$children}} @endif @if($children == 0)
                                        No Child
                                        @elseif($children == 1)
                                        Child
                                        @elseif($children > 1)
                                        Children
                                        @endif</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="mt-4 mb-4 row">
                <div class="col-md-4 col-sm-6 mb-3 mb-md-0">
                    <a href="{{url('view-itinerary')}}" name="" class="btn btn-white btn-block active mb-3 mb-sm-0">VIEW ITINERARY</a>
                </div>
                <div class="col-md-4 col-sm-6 mb-3 mb-md-0">
                    <a href="{{url('proposed-itinerary-pdf')}}" class="btn btn-white btn-block mb-3 mb-sm-0" target="_blank">PRINT / SHARE ITINERARY</a>
                </div>
                <div class="col-md-4 col-sm-6 mb-3 mb-md-0">
                    <a href="{{url('payment-summary')}}" name="" class="btn btn-white btn-block mb-3 mb-sm-0">BOOK ITINERARY</a>
                </div>
            </div>
            <hr>
             <div class="viewItinerary-city">
                                <div class="jcarousel-wrapper profile-steps">
                                    <div class="jcarousel">
                                        <ul class="accomodationImg-list nav nav-tabs nav-fill border-0">
                                            @foreach ($data['itinerary'] as $leg)
                                            <li>
                                                <a href="javascript:void(0)" class="nav-link">


<span class="circle d-flex align-items-center justify-content-center">{{$leg['city']['default_nights']}}</span>
<span class="d-block"><strong>{{str_limit($leg['city']['name'], 18)}}</strong><br>{{$leg['city']['country']['name']}}</span>
                                                 </a>
                                            </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                    <a href="#" class="jcarousel-control-prev" data-jcarouselcontrol="true"><i class="fa fa-angle-left"></i></a>
                                    <a href="#" class="jcarousel-control-next" data-jcarouselcontrol="true"><i class="fa fa-angle-right"></i></a>
                                </div>
                            </div>
            <div class="locations_map pt-3 pt-5 pb-5 ">
                <div id="map1" style="height: 400px;"></div>
            </div>
            <section class="tours-wrapper">
                @if (session()->get('search'))
                @php $i=1; @endphp
                @foreach ($data['itinerary'] as $key => $leg)
                <div class="tripDetails mb-3">
                    <div class="card panel border-0 p-3">
                        <div class="row">
                            <div class="col-sm-7 col-xl-8">
                                <h5 class="font-weight-bold">{{$leg['city']['name']}}, {{$leg['city']['country']['name']}}</h5>
                                <div class="media">
                                    <i class="ic-calendar mr-2"></i>
                                    <div class="media-body pb-2 pt-1 mb-0">
                                        {{  !empty($leg['city']['date_from']) ? date('d F Y', strtotime($leg['city']['date_from'])) :'N/A'}} - {{!empty($leg['city']['date_to']) ? date('d F Y', strtotime($leg['city']['date_to'])):'N/A'}}
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-5 col-xl-4">
                                <div class="row align-items-end">
                                    <div class="col-xl-9 col-sm-8 text-left text-sm-right">
                                        <ul class="list-unstyled mb-0">
                                            <li class="list-inline-item"><i class="ic-local_hotel fa-lg"></i></li>
                                            <li class="list-inline-item"><i class="ic-local_activity fa-lg"></i></li>
                                            <li class="list-inline-item"><i class="ic-directions_bus fa-lg"></i></li>
                                        </ul>
                                    </div>
                                    <div class="col-xl-3 col-sm-4 text-left text-sm-center mt-2 mt-md-0 border-left">
                                        <h2 class="font-weight-bold mb-0">0{{$leg['city']['default_nights']}}</h2>
                                        <?php echo $leg['city']['default_nights'] > 1 ? 'Days' : 'Day' ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="clearfix">
                            <div class="accomodation-img">
                                @if(!empty($leg['city']['image']))
                                <img src="http://cms.eroam.com/{{$leg['city']['image'][0]['small'] }}" alt="" class="img-fluid" />
                                @else
                                <img src="{{ url('assets/images/no-image1.jpg') }}" alt="" class="img-fluid" />
                                @endif
                            </div>
                            <div class="accomodation-details lh-condensed">
                                <p>{!! substr($leg['city']['description'], 0, 500) !!}</p>
                                @if((isset($leg['hotel']) && !empty($leg['hotel'])) || (isset($leg['activities']) && !empty($leg['activities'])) || (isset($leg['transport']) && !empty($leg['transport'])))
                                <p class="mt-5"><a href="javascript://" class="text-dark trip-viewmore" data-id="trip{{$i}}"><i class="ic-expand_more mr-1"></i> Click to view detailed itinerary</a></p>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="tripDetails-wrapper" id="trip{{$i}}">
                        @if(isset($leg['hotel']) && !empty($leg['hotel']))
                        <div class="tripDetails-container mb-3 mt-3">
                            <div class="tripDetails-top">
                                <div class="row">
                                    <div class="col-sm-8">
                                        <span class="mr-2"><i class="ic-local_hotel fa-lg"></i></span> <strong>ACCOMMODATION</strong>
                                    </div>
                                    <div class="col-sm-4 text-left text-sm-right">
                                        <!--a href="#" data-toggle="modal" data-target="#editTripModal"><i class="ic-create"></i></a!>
                                        <a href="#"><i class="ic-block"></i></a-->
                                    </div>
                                </div>
                            </div>
                            <div class="tripDetails-bottom">
                                <div class="table-responsive">
                                    <table class="table mb-0">
                                        <thead class="thead-light">
                                            <tr>
                                                <th>Date</th>
                                                <th>Name</th>
                                                <th>Location</th>
                                                <th>Check In</th>
                                                <th>Check Out</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr class="tourTripName1" data-id="tourTrip1{{$i}}">
                                                <td>{{date('d M Y', strtotime($leg['hotel']['checkin']))}}</td>
                                                <td>{{$leg['hotel']['name']}}</td>
                                                <td>{{ ($leg['hotel']['provider'] == 'expedia') ? $leg['hotel']['address1'] : $leg['hotel']['address_1']}}</td>
                                                <td>{{date('d M Y', strtotime($leg['hotel']['checkin']))}}</td>
                                                <td>{{date('d M Y', strtotime($leg['hotel']['checkout']))}}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        @php
                        //echo "<pre>";print_r($leg);exit;
                        @endphp
                        <div class="itinerary_hotel_list pl-3 pr-3 pb-3 pt-3 mb-3" id="tourTrip1{{$i}}" style="display: none;">
                            <div class="border-bottom hotlename pb-2   mb-3 ">
                                <div class="row">
                                    <div class="col-sm-7 col-xl-8">
                                    <h4>{{$leg['hotel']['name']}}</h4>
                                    <div class="media">
                                        <i class="ic-place"></i>
                                        <div class="media-body pb-3 mb-0">
                                        Location: {{$leg['city']['country']['name']}}, {{$leg['city']['name']}} / {{ ($leg['hotel']['provider'] == 'expedia') ? $leg['hotel']['address1'] : $leg['hotel']['address_1']}}
                                        </div>
                                    </div>
                                    </div>
                                    <div class="col-sm-5 col-xl-4">
                                    <div class="row align-items-end">
                                        <div class="col-xl-9 col-sm-8 text-left text-sm-right">
                                            <?php
                                            if($leg['hotel']['provider'] == 'expedia'){
                                                $eroamPercentage = Config::get('constants.ExpediaEroamCommissionPercentage');
                                                $singleRate = '@nightlyRateTotal';
                                                $singleRate = $leg['hotel']['RoomRateDetailsList']['RoomRateDetails']['RateInfos']['RateInfo']['ChargeableRateInfo'][$singleRate];

                                                $singleRate = ($singleRate * $eroamPercentage) / 100 + $singleRate;
                                                $subTotal = $singleRate;
                                                $taxes = 0;

                                                $totalNights = $leg['hotel']['RoomRateDetailsList']['RoomRateDetails']['RateInfos']['RateInfo']['ChargeableRateInfo']['NightlyRatesPerRoom']['@size'];

                                                if (isset($leg['hotel']['RoomRateDetailsList']['RoomRateDetails']['RateInfos']['RateInfo']['taxRate'])):

                                                $taxes = $leg['hotel']['RoomRateDetailsList']['RoomRateDetails']['RateInfos']['RateInfo']['taxRate'];
                                                $taxesPerDay = $taxes / $totalNights;

                                                endif;

                                                $ratePerDay = $leg['hotel']['RoomRateDetailsList']['RoomRateDetails']['RateInfos']['RateInfo']['ChargeableRateInfo']['@averageBaseRate'];

                                                $ratePerDay = number_format(($ratePerDay * $eroamPercentage) / 100 + $ratePerDay + $taxesPerDay, 2);

                                                $selectedRate = number_format($subTotal + $taxes, 2);
                                                }
                                            else{
                                                $price = min(array_column($leg['hotel']['price'], 'price'));
                                                $selectedRate = $price;
                                            }
                                            ?>
                                            From <strong>${{$currency}} {{$selectedRate}}</strong> Per Night
                                        </div>
                                        <div class="col-xl-3 col-sm-4 text-left text-sm-center mt-2 mt-md-0 border-left">
                                        <h2 class="font-weight-bold mb-0">{{$leg['hotel']['nights']}}</h2>
                                        Nights
                                        </div>
                                    </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12 col-sm-4 col-md-4 col-lg-4 col-xl-3 ">
                                    @if(!empty($leg['city']['image']))
                                        <img src="https://cms.eroam.com/{{$leg['city']['image'][0]['small']}}" alt="" />
                                    @else
                                        <img src="{{ url('assets/images/no-image1.jpg') }}" alt="" />
                                    @endif
                                </div>
                                <div class="col-12 col-sm-8 col-md-8 col-lg-8 col-xl-9 ">
                                    <div class="rating">
                                        <span class="d-inline-block"><i class=" ic-star fa-lg"></i></span>
                                        <span class="d-inline-block"><i class=" ic-star fa-lg"></i></span>
                                        <span class="d-inline-block"><i class=" ic-star fa-lg"></i></span>
                                        <span class="d-inline-block"><i class=" ic-star fa-lg"></i></span>
                                        <span class="d-inline-block"><i class=" ic-half_star fa-lg"></i></span>
                                    </div>
                                    <div class="row mt-3" style="margin-left: -2px;">
                                        <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                            @isset($leg['hotel']['RoomRateDetailsList']['RoomRateDetails']['RoomType']['descriptionLong'])
                                            {!! $leg['hotel']['RoomRateDetailsList']['RoomRateDetails']['RoomType']['descriptionLong'] !!}
                                            @endisset
                                        </div>
                                    </div>
                                    <div class="row" style="margin-left: -2px;">
                                        <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                            @if(isset($leg['hotel']['shortDescription']))
                                            {!! $leg['hotel']['shortDescription'] !!}
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        @endif
                        @if(isset($leg['activities']) && !empty($leg['activities']))
                        <div class="tripDetails-container mb-3">
                            <div class="tripDetails-top">
                                <div class="row">
                                    <div class="col-sm-8">
                                        <span class="mr-2"><i class="ic-local_activity"></i></span> <strong>TOURS / ACTIVITIES</strong>
                                    </div>
                                    <div class="col-sm-4 text-left text-sm-right">
                                        <!--a href="#" class="text-dark"><i class="ic-create"></i></a>
                                        <a href="#" class="text-dark"><i class="ic-block"></i></a!-->
                                    </div>
                                </div>
                            </div>
                            <div class="tripDetails-bottom">
                                <div class="table-responsive">
                                    <table class="table mb-0">
                                        <thead>
                                            <tr>
                                                <th>Date</th>
                                                <th>Name</th>
                                                <th>Location</th>
                                                <th>Duration</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($leg['activities'] as $activity)
                                            <tr>
                                                <td>{{date('d M Y', strtotime($activity['date_selected']))}}</td>
                                                <td>{{$activity['name']}}</td>
                                                <td>{{ $leg['city']['name']}}, {{$leg['city']['country']['name']}}</td>
                                                <td>{{!empty($activity['duration1'])? ucwords($activity['duration1']):'N/A'}}</td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        @endif
                        @if(isset($leg['transport']) && !empty($leg['transport']))
                        <div class="tripDetails-container">
                            <div class="tripDetails-top">
                                <div class="row">
                                    <div class="col-sm-8">
                                        <span class="mr-2"><i class="ic-directions_bus fa-lg"></i></span> <strong>TRANSPORT</strong>
                                    </div>
                                    <div class="col-sm-4 text-left text-sm-right">
                                        <!--a href="#" class="text-dark"><i class="ic-create"></i></a>
                                        <a href="#" class="text-dark"><i class="ic-block"></i></a!-->
                                    </div>
                                </div>
                            </div>
                            <div class="tripDetails-bottom">
                                <div class="table-responsive">
                                    <table class="table mb-0">
                                        <thead>
                                            <tr>
                                                <th>Date</th>
                                                <th>Name</th>
                                                <th>Departure Time</th>
                                                <th>Arrival / Time</th>
                                                <th>Duration</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @php
                                                $travelDate = explode('<br/>', $leg['transport']['departure_text']);
                                                $travelDate = explode('@', $travelDate[1]);
                                                $departure_time = explode('<br/>', $leg['transport']['departure_text']);
                                                $arrival_time = explode('<br/>', $leg['transport']['arrival_text']);
                                            @endphp
                                            <td><?php echo date('d M Y', strtotime($travelDate[0])); ?></td>
                                            <td>{{ $leg['transport']['transport_name_text'] }}</td>
                                            <td>{{$departure_time[1]}}</td>
                                            <td>{{$arrival_time[1]}}</td>
                                            <td>{{ ucwords(str_replace('+', '', $leg['transport']['duration'])) }}</td>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        @endif
                    </div>
                </div>
                @php $i++; @endphp
                @endforeach
                @endif
            </section>
        </div>
    </div>
</div>
<input type="hidden" id="map-data" value="{{ json_encode( Session::get( 'map_data' ) ) }}">
<input type="hidden" id="all-cities" value="{{ json_encode(getAllCities()) }}">
@endsection
@push('scripts')
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD14SXDwQRXOfUrldYNP-2KYVOm3JjZN9U&libraries=places,geometry,drawing&v=3&client=gme-adventuretravelcomau"></script>
    <script src="{{url('js/itinerary/common.js')}}"></script>
    <script src="{{url('js/booking-summary.js')}}"></script>
    <script src="{{url('js/map/jquery.slimscroll.js')}}"></script>
    <script src="js/jquery.jcarousel.min.js"></script>
    <script src="js/jcarousel-custom.js"></script>
    <script src="{{url('js/markerlabel.js') }}"></script>
    <script src="{{url('js/eroam-map_managetrip.js')}}"></script>
    <script src="{{url('js/moment.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            tripHideShow();
            tripHotelHideShow();
        });

        function tripHideShow() {
            $('.trip-viewmore').click(function() {
                var href_value = $(this).attr('data-id');
                $('#' + href_value).toggle();
                $('#' + href_value).toggleClass("open");
                $(this).toggleClass("active");
            });
        }

        function setupLabel() {
            if ($('.label_radio input').length) {
                $('.label_radio').each(function() {
                    $(this).removeClass('r_on');
                });
                $('.label_radio input:checked').each(function() {
                    $(this).parent('label').addClass('r_on');
                    var inputValue = $(this).attr("value");
                    //alert(inputValue);
                    $("." + inputValue).addClass('mode-block');
                    $("." + inputValue).siblings().removeClass('mode-block');
                });
            };

            if ($('.label_check input').length) {
                $('.label_check').each(function() {
                    $(this).removeClass('c_on');
                });
                $('.label_check input:checked').each(function() {
                    $(this).parent('label').addClass('c_on');
                });
            };

        };
        $(window).load(eMap.init);

        function map() {
            // When the window has finished loading create our google map below
            google.maps.event.addDomListener(window, 'load', init);

            function init() {
                // Basic options for a simple Google Map
                // For more options see: https://developers.google.com/maps/documentation/javascript/reference#MapOptions
                var mapOptions = {
                    // How zoomed in you want the map to start at (always required)
                    zoom: 11,

                    // The latitude and longitude to center the map (always required)
                    center: new google.maps.LatLng(40.6700, -73.9400), // New York

                    // How you would like to style the map.
                    // This is where you would paste any style found on Snazzy Maps.
                    styles: [{
                        "featureType": "administrative",
                        "elementType": "labels.text.fill",
                        "stylers": [{
                            "color": "#444444"
                        }]
                    }, {
                        "featureType": "landscape",
                        "elementType": "all",
                        "stylers": [{
                            "color": "#f2f2f2"
                        }]
                    }, {
                        "featureType": "poi",
                        "elementType": "all",
                        "stylers": [{
                            "visibility": "off"
                        }]
                    }, {
                        "featureType": "road",
                        "elementType": "all",
                        "stylers": [{
                            "saturation": -100
                        }, {
                            "lightness": 45
                        }]
                    }, {
                        "featureType": "road.highway",
                        "elementType": "all",
                        "stylers": [{
                            "visibility": "simplified"
                        }]
                    }, {
                        "featureType": "road.arterial",
                        "elementType": "labels.icon",
                        "stylers": [{
                            "visibility": "off"
                        }]
                    }, {
                        "featureType": "transit",
                        "elementType": "all",
                        "stylers": [{
                            "visibility": "off"
                        }]
                    }, {
                        "featureType": "water",
                        "elementType": "all",
                        "stylers": [{
                            "color": "#46bcec"
                        }, {
                            "visibility": "on"
                        }]
                    }]
                };

                // Get the HTML DOM element that will contain your map
                // We are using a div with id="map" seen below in the <body>
                var mapElement = document.getElementById('map');

                // Create the Google Map using our element and options defined above
                var map = new google.maps.Map(mapElement, mapOptions);

                // Let's also add a marker while we're at it
                var marker = new google.maps.Marker({
                    position: new google.maps.LatLng(40.6700, -73.9400),
                    map: map,
                    title: 'eRoam!'
                });
            }
        }

        function tripHotelHideShow(){
            $('.tourTripName').click(function(){
                var href_value = $(this).attr('data-id');
                $('#'+href_value).toggle();
            });
        }
   </script>
@endpush