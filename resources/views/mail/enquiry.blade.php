<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
	<title>eRoam</title>
</head>
<style type="text/css">
	.pcolor{
		color:#505050 !important;
	}
</style>
<body style="margin: 0px; background: #e3e5e7;">
	<?php 
		$logo = getDomainLogo();
		if(!$logo) {
			$logo = "http://dev.eroam.com/images/email-logo-footer.png";
		}
	?>
    <table width="100%" style="background: #e3e5e7">
        <tr>
            <td>
				<table style="margin:0 auto; background: #ffffff;" width="760px" border="0" cellspacing="0" cellpadding="0">
                    <thead>
                        <tr>
                            <th style="background-color: #212121; padding-top:20px; padding-bottom: 20px; text-align: center;">
                                <a href="#"><img src="<?php echo $logo; ?>" width="180px" alt="" /> </a>
                             </th>
                        </tr>
					</thead>
					<tbody>
						<tr>
							<td style="background: #fafafa; border-bottom: 1px solid #f0f0f0; padding-top: 5px; padding-bottom: 5px;">    
								<table width="100%">
									<tr>
                                        <td style="text-align: center">
                                            <img src="http://dev.eroam.com/images/user_icon.png" alt="" />
										</td>
                                    </tr>
                                    <tr>
										<td style="text-align: center">
                                           <p style="font-family: Arial; font-size: 18px; color: #212121; margin-top: 0px; margin-bottom: 0px;  "><strong> Hi, User!</strong></p>
										</td>
									</tr>
								</table>
							</td>
                        </tr>
                        <tr>
							<td>
								<table width="90%;" style="margin: 10px auto 0 auto; font-size: 15px; border-collapse: collapse;">
                                    <tbody>
										<tr>
                                            <td style="color:#505050 !important;">
												<h3 style="margin-bottom: 30px;">Hi Admin,</h3>
												<p class="pcolor">A new user has contacted us. Kindly find below details.</p>
												<p class="pcolor"> Product : <strong>{{ $product }}</strong></p>
												<p class="pcolor"> First Name : {{ $first_name }}</p>
												<p class="pcolor"> Family Name : {{ $family_name }}</p>
												<p class="pcolor"> Email : {{ $email }}</p>
												<p class="pcolor"> Phone : {{ $phone }}</p>
												<p class="pcolor"> Postcode : {{ $postcode }}</p>
												<p class="pcolor"> Travel Date : {{ (!empty($travel_date) ? date('d-m-Y',strtotime($travel_date)) : '')}}</p>
												<p class="pcolor"> Comments : {{ $comments }}</p>
											</td>
                                        </tr>
										<tr>
											<td>
												<p style="margin-top: 50px;color: #505050;"><strong>Thank You</strong>, <br/>eRoam</p>
											</td>
										</tr>
                                    </tbody>
								</table>
							</td>
                        </tr>
					</tbody>
					<tfoot>
                        <tr>
                            <td style="background-color: #394951; padding-top:6px; padding-bottom: 6px; padding-right: 6px; padding-left: 6px; text-align: center;">
								<table style="width: 100%;">

                                    <tr>
                                        <td style="font-family: Arial; font-size: 14px;"> <a href="#"><img src="<?php echo $logo; ?>" alt="" /> </a></td>
                                        <td style="color: #fff;font-size: 12px; text-align: right; font-family: Arial; ">Powered by eRoam &copy; Copyright 2018 - 2019. All Rights Reserved. Patent pending AU2016902466</td>
									</tr>
								</table>
							</td>
                        </tr>
					</tfoot>
				</table>
			</td>
		</tr>
	</table>
</body>
</html>
