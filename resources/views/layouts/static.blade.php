@include( 'layouts.partials.header' )
	<section class="inner-page content-wrapper">
		@yield( 'content' )
	</section>
@include( 'layouts.partials.footer' )